const puppeteer = require('puppeteer');
const {Storage} = require('@google-cloud/storage');
const Url = require('url');

const metricsBucket = process.env.BUCKET_METRICS;

let gcs;


/**
 * Cloud Function entry point, HTTP trigger.
 * Loads the requested URL via Puppeteer, captures page performance
 * metrics, and writes to GCS buckets.
 *
 * @param {Object} req Cloud Function request context.
 * @param {Object} res Cloud Function response context.
 */
exports.trace = async (req, res) => {
  const url = getUrl(req);
  if (!url || !url.hostname) {
    console.error('Valid URL to trace not specified')
    return res.status(400).send('Please specify a valid URL to trace');
  }

  let browser;
  const filename = new Date().toISOString();
  try {
    browser = await puppeteer.launch({
      headless: true,
      args: ['--no-sandbox']
    });
    const page = await browser.newPage();
    const client = await page.target().createCDPSession();
    await client.send('Performance.enable');

    // browse to the page, capture and write the performance metrics
    console.log('Fetching url: '+url.href);
    await page.goto(url.href, {
      'waitUntil' : 'networkidle0'
    });
    const performanceMetrics = await client.send('Performance.getMetrics');
    options = createUploadOptions('application/json', page.url());
    await writeToGcs(metricsBucket, filename, JSON.stringify(performanceMetrics), options);
    // [END puppeteer-block]

    res.status(200).send({
      url: page.url,
      filename: filename
    });
  } catch (e) {
    console.error('Caught Error: '+e);
    res.status(500).send(e);
  } finally {
    if (browser) {
      await browser.close();
    }
  }
}

async function writeToGcs(bucketName, filename, content, options) {
  gcs = gcs || new Storage();
  const bucket = gcs.bucket(bucketName);
  const file = bucket.file(filename);
  const gcs_filename = `gs://${bucket.name}/${file.name}`

  const stream = file.createWriteStream(options);
  return new Promise((resolve, reject) => {
    stream.end(content);
    stream.on('error', (err) => {
      console.error('Error writing GCS file: ' + err);
      reject(err);
    });
    stream.on('finish', () => {
      console.log('Created object: '+gcs_filename);
      resolve(200);
    });
  });
}

function createUploadOptions(contentType, url) {
  return {
    resumable: false,
    metadata: {
      contentType: contentType,
      metadata: {
        pageUrl: url,
      }
    }
  };
}

function getUrl(req) {
  if (req.query.url || req.body.url) {
    return Url.parse(req.query.url || req.body.url);
  }
  try {
    return Url.parse(JSON.parse(req.body).url);
  } catch (e) {}
}
